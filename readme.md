- composer install

- cp .env.sample .env

- php artisan vendor:publish

- rename config/custom-rename.php to config/custom.php. Change token value and else

- If you want to use https, add this line to app/Providers/AppServiceProvider.php on boot section (enabled by default):
```
\Illuminate\Support\Facades\URL::forceScheme('https');
```
- if use http, uncomment that line.
- 