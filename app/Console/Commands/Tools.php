<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class Tools extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'tools {domain} {--keyword} {--deep}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//
	}


	public static function saveKeyword($domain, $keyword, $deep) {
		$opts = ["http" => ['method' => 'GET','header' => 'X-Authorization: '.config('custom.token')]];
		$context = stream_context_create($opts);
		try {
			$lines = explode(PHP_EOL, file_get_contents('http://pool.wis.nu/wallpaper/getPinKeyword/'.urlencode($keyword), false, $context));

			print('just ignore errors below.. we have sooo many other spares..'.PHP_EOL.PHP_EOL);
			$accumulate = array();
			foreach ($lines as $line) {

				try {
					$iterate = explode(PHP_EOL, file_get_contents('http://pool.wis.nu/wallpaper/getPinKeyword/'.urlencode($line), false, $context));
					print($line.' ok'.PHP_EOL);
					foreach ($iterate as $i) {
						$accumulate[] = $i;

					}
				} catch (\Exception $e) {
					print('fail on '.$line.PHP_EOL);
				}
			}
			Storage::put('keywords/'.$domain.'.txt', implode(PHP_EOL, array_unique($accumulate)));

			print("ok done...\nYou have ".count(array_unique($accumulate))." new lines of keyword\n");
		} catch (\Exception $e) {
			print("Sorry! We currently don't have enough keyword data for that request. Try another keyword.\n");
		}
	}


}
