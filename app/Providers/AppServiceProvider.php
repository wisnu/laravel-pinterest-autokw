<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Contracts\Filesystem\Filesystem;
use League\Glide\Responses\LaravelResponseFactory;
use League\Glide\ServerFactory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\URL::forceScheme('https');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('League\Glide\Server', function($app){

            $filesystem = $app->make('Illuminate\Contracts\Filesystem\Filesystem');
            $factory = $app->make('League\Glide\Responses\LaravelResponseFactory');

            return \League\Glide\ServerFactory::create([
                'response' => $factory,
                'source' => Storage::disk(config('custom.imageloc'))->getDriver(),
                'cache' => Storage::disk(config('custom.imageloc'))->getDriver(),
//                 'watermarks' => $filesystem->getDriver(),
/*
                'source_path_prefix' => 'img',
                'cache_path_prefix' => 'img/.cache',
*/
//                 'watermarks_path_prefix' => 'photos/watermarks',
                // 'base_url' => 'photos',
                ]);
        });
    }
}
