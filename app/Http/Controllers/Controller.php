<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Cache;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use Intervention\Image\ImageManagerStatic as Image;
use HTMLMin\HTMLMin\HTMLMin;
use HTMLMin\HTMLMin\Minifiers\BladeMinifier;
use HTMLMin\HTMLMin\Minifiers\CssMinifier;
use HTMLMin\HTMLMin\Minifiers\HtmlMinifier;
use HTMLMin\HTMLMin\Minifiers\JsMinifier;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	use SEOToolsTrait;

	public function __construct() {
		$lines_name = $_SERVER['HTTP_HOST'].'_lines';
		$keys_name = $_SERVER['HTTP_HOST'].'_keys';

		if (Storage::exists('keywords/'.$_SERVER['HTTP_HOST'].'.txt')) {
			$this->$lines_name = \file(Storage::url('keywords/'.$_SERVER['HTTP_HOST'].'.txt'), \FILE_IGNORE_NEW_LINES | \FILE_SKIP_EMPTY_LINES);

			$this->$keys_name = [];
			foreach ($this->$lines_name as $l) {
				$this->$keys_name = array_add($this->$keys_name, str_replace('.', '', $l), str_slug($l));
			}
		}
	}


	/*	INIT	*/

	public function saveKeyword() {
		$opts = ["http" => ['method' => 'GET','header' => 'X-Authorization: '.config('custom.token')]];
		$context = stream_context_create($opts);
		try {
			$file = file_get_contents('http://pool.wis.nu/wallpaper/getPinKeyword/'.urlencode(Input::get('keyword')), false, $context);
			Storage::put('keywords/'.$_SERVER['HTTP_HOST'].'.txt', $file);
			return back()->with('success', 'keyword inserted.');
		} catch (\Exception $e) {
			return \Response::make(dd(array('status' => 'failure', 'message' => 'Sorry! We currently don\'t have enough keyword data for that request. Try another keyword.')), 200, $headers);
		}
	
	}


	/*	INDEX	*/

	public function index() {
		$lines_name = $_SERVER['HTTP_HOST'].'_lines';
		if (!Storage::exists('keywords/'.$_SERVER['HTTP_HOST'].'.txt')) {
			return view('init');
		} else {
			$single   				= random_terms($this->$lines_name, 2);
			$data    				= saveData($single[0]);
			$data['keyword'] 		= array_random($this->$lines_name, 555);

		    return view('index')->with('data', $data);
// 		    dd($data);
		}
	}


	/*	SEARCH	*/

	public function search($query) {
		$keys_name = $_SERVER['HTTP_HOST'].'_keys';
		$lines_name = $_SERVER['HTTP_HOST'].'_lines';
		if (contains(explode(',', config('filters.badwords')), $query)) {
			// return abort(404);
			return redirect()->route('index');
		} else {
			if ($keyword 			= array_keys($this->$keys_name, $query, false)) {   // check slug exist in keywords.txt
				$data 				= saveData($query);
				$data['keyword'] 	= array_slice($this->$lines_name, rand(0, count($this->$lines_name)), 55);
			} else {
				    return abort(500);
// 				return redirect()->route('index');
			}
		}
		return view('search')->with('data', $data);


	}



	/*	ATTACHMENT	*/

	public function attachment($query, $subquery) {
		$keys_name = $_SERVER['HTTP_HOST'].'_keys';
		$lines_name = $_SERVER['HTTP_HOST'].'_lines';
		if (contains(explode(',', config('filters.badwords')), $query)) {
			return redirect()->route('index');
		} else {
			if ($keyword 			= array_keys($this->$keys_name, $query, false)) {   // check slug exist in keywords.txt
				$data 				= saveData($query, $subquery);
				$data['keyword'] 	= array_slice($this->$lines_name, rand(0, count($this->$lines_name)), 55);
			} else {
				return redirect()->route('index');
			}
		}

		return view('attachment')->with('data', $data);
// 		dd($data);

	}


	/*	PAGE	*/

	public function page($pagename)
	{
		$lines_name = $_SERVER['HTTP_HOST'].'_lines';
		$data['page_title']   		= title_case(str_replace('-', ' ', $pagename));
		$data['path']         		= __FUNCTION__; //page
		$data['title']        		= title_case(array_slice($this->$lines_name, rand(0, count($this->$lines_name)), 30)[0]);
		$data['keyword'] 			= array_slice($this->$lines_name, rand(0, count($this->$lines_name)), 20);

		return view('page')->with('data', $data);
// 		return json_encode($data);
	}


	/*	IMG PROXY	*/


	public function thumb($w, $h, $catchall) {
		Image::configure(array('driver' => 'imagick'));
		$img = Image::make(imgproxy($catchall, $w, $h));
		header('Content-Type: image/jpg');
		return $img->response();
	}
	
	public function full($catchall) {
		Image::configure(array('driver' => 'imagick'));
		$img = Image::make(imgproxy($catchall));
		header('Content-Type: image/jpg');
		return $img->response();
/*
		$imginfo = getimagesize(imgproxy($catchall));
		header('Content-type: '.$imginfo['mime']);
		readfile(imgproxy($catchall));
		die();
*/
	}



}
