<?php
date_default_timezone_set('America/Los_Angeles');

require __DIR__.'/../../vendor/autoload.php';
use Intervention\Image\ImageManager;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

if (!function_exists('theImage')) {

	/**
	 * description
	 *
	 * @param
	 * @return
	 */
	function theImage($size='full', $name, $source, $w=null, $h=null)
	{
		if (config('custom.image') == 'saved') {
			$filename = $size.'/'.breakName($name).'/'.str_slug($name).'.jpg';
			if (Storage::disk(config('custom.imageloc'))->has($filename)) {
				return url("img/$filename");
			} else {	// didn't has it
				saveImage($size, $name, $source, $w, $h);
				return url("img/$filename");
			}
		} 
		
		//	image = wrapped
		elseif (config('custom.image') == 'wrapped') {
			if ($size == 'full') {
				return url($size.'/'.stripProtocol($source, $w, $h));
			} else {
				return url("$size/$w/$h/".stripProtocol($source, $w, $h));
			}
		}
		
		//	image = hotlink
		else {
				return $source;
		}
	}
}


if (!function_exists('saveImage')) {
	function saveImage($size, $name, $source, $w=null, $h=null) {
		$filename = breakName($name).'/'.str_slug($name).'.jpg';

		Image::configure(array('driver' => 'imagick'));
		$full = Image::make(imgproxy(stripProtocol($source)));
		$thumb = Image::make(imgproxy(stripProtocol($source), 300, 180));
		Storage::disk(config('custom.imageloc'))->put('full/'.$filename, $full->stream(), 'public');
		Storage::disk(config('custom.imageloc'))->put('thumb/'.$filename, $thumb->stream(), 'public');
	}
}

if (!function_exists('breakName')) {
	function breakName($name) {
		if (empty($name)) {
			$name = 'none';
		}
		$slug = str_slug($name);
		return $slug[0].'/'.$slug[1];
	}
}

if (! function_exists('stripProtocol')) {
	function stripProtocol($source) {
		return explode('//', $source)[1];
	}
}

if (! function_exists('getFullImg')) {
	function getFullImg($source) {
		$ch = curl_init(explode('?', $source)[0]);
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); // follow redirects
		curl_setopt($ch, CURLOPT_AUTOREFERER, 1); // set referer on redirect
		curl_exec($ch);
		$target = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		curl_close($ch);
		if ($target) {
/*
			if (strpos($target, 'http') === false) {
				return 'http://'.$_SERVER['HTTP_HOST'].'/img/http:'. $target;
			} else {
				return 'http://'.$_SERVER['HTTP_HOST'].'/img/'. $target;
			}
*/
			return $target;
		} else {
			return explode('?', $source)[0];
		}
	}
}

if (! function_exists('imgproxy')) {
	function imgproxy($source, $w=null, $h=null) {
		if (config('custom.imgproxy') == 'images.weserv.nl') {
			if (isset($w)) {

				return 'https://images.weserv.nl/?url='.($source).'&w='.$w.'&h='.$h.'&t=square&a=top&q=80&errorredirect=ssl:'.urlencode('dummyimage.com/'.$w.'x'.$h.'/f2f2f2/ff5f46.jpg&text='.$_SERVER['HTTP_HOST']);

			} else {

				return 'https://images.weserv.nl/?url='.($source).'&errorredirect=ssl:'.urlencode('dummyimage.com/'.$w.'x'.$h.'/f2f2f2/ff5f46.jpg&text='.$_SERVER['HTTP_HOST']);

			}
		}
		if (strpos(config('custom.imgproxy'), 'wp')) {
			if (isset($w) ) {

				return 'https://'.config('custom.imgproxy').'/'.($source).'?resize='.$w.','.$h.'&strip=all&quality=80';

			} else {

				return 'https://'.config('custom.imgproxy').'/'.($source).'?strip=all&quality=80';

			}
		}

	}
}
