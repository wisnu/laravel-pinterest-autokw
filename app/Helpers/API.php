<?php
date_default_timezone_set('America/Los_Angeles');

require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Cache;

if (! function_exists('saveData')) {
	function saveData($query, $subquery = null) {
		$opts = array(
			"http" => array(
				'method' => 'GET', 
				'header' => 'X-Authorization: '.config('custom.token'),
// 				'proxy' => collect(config('custom.proxy'))->random()
			));
		$context = stream_context_create($opts);


		//get group of datas

/*
		foreach ($query as $que) {
			$cache_key = str_slug($que);
			if (Cache::has($cache_key)) {
				$cache = Cache::get($cache_key);
			} else {
				$pinterestData = json_decode(file_get_contents('http://pool.wis.nu/content/raw/'.$que, false, $context), true);

				// Get or save search result from cache
				if (isset($pinterestData->pins)) {
					Cache::put($cache_key, $pinterestData->pins, 14400);
				//     $cache = Cache::get($cache_key);
				}
				$cache = $pinterestData;
			}
			$json['data'][] = $cache;


			$relatedCache = 'related_'.str_slug($que);
			if (Cache::has($relatedCache)) {
				$relatedCacheKey = Cache::get($relatedCache);
			} else {
				$relatedGoogle = json_decode(file_get_contents('http://suggestqueries.google.com/complete/search?client=firefox&q='.str_replace(' ', '%20' , $que).'&hl=en',false, $context));

				$related = array();
				foreach ($relatedGoogle[1] as $r1) {
					Cache::put($relatedCache, $relatedGoogle[1], 14400);
					$relatedCacheKey[] = ucfirst($r1);
				}

			}

			if (isset($relatedCacheKey))  {
				$json['related'] = array_flatten($relatedCacheKey);
			} else {
				$json['related'] = $query;
			}
		}
*/

			if (Cache::has('raw_'.$query)) {
				$cache = Cache::get('raw_'.$query);
			} else {
				$pinterestData = json_decode(file_get_contents('http://pool.wis.nu/content/raw/'.$query, false, $context), true);

				// Get or save search result from cache
				if (isset($pinterestData->pins)) {
					Cache::put('raw_'.$query, $pinterestData, 14400);

				}
				$cache = (array)$pinterestData;
			}

		$json['data'] = $cache['pins'];
		$json['related'] = $cache['sub_keywords'];

		if (isset($query)) {
			$json['query'] = $query;
		
		}

		if (isset($subquery)) {
			$json['id'] = array_search($subquery, array_column($json['data'], 'slug'));
			$json['subquery'] = $subquery;
		}

		if (config('custom.content') == true) {
			$json['content'] = 'ada';
		}
			return $json;

	}
}


if (! function_exists('API')) {
	function API($request, $keyword = null) {
		if ($request == 'getKeyword') {
			$url = 'http://pool.wis.nu/wallpaper/getPinKeyword/'.config('custom.keyword');
		}
		if ($request == 'getSearch') {
			$url = 'http://pool.wis.nu/wallpaper/pinterest/'.$keyword;
		}

		$header = array(
			'X-Authorization: '.config('custom.token')
		);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		if (!$html = curl_exec($curl)) {
			$html = 'noo Jon Snow nooo';
		}
		curl_close($curl);
		return $html;
		//   return \Response::make((string)$html, 200, ['Content-type' => 'text/plain']);

	}
}

if (! function_exists('poolAPI')) {
	function poolAPI($request, $keyword = null) {
		if ($request == 'getKeyword') {
			$url = 'http://pool.wis.nu/wallpaper/getPinKeyword/'.urlencode(Input::get('keyword'));
		}
		if ($request == 'getSearch') {
			$url = 'http://pool.wis.nu/wallpaper/pinterest/'.$keyword;
		}
		
		$opts = ["http" => ['method' => 'GET','header' => 'X-Authorization: '.config('custom.token')]];
		$context = stream_context_create($opts);

		$file = file_get_contents($url, false, $context);
	}
}
