<?php
date_default_timezone_set('America/Los_Angeles');

require __DIR__.'/../../vendor/autoload.php';

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Cache;

if (! function_exists('attachment_url')) {
	function attachment_url( $query, $subquery ) {
		return url(str_slug($query).'/'.str_slug($subquery).'.html');
	}

}

if (! function_exists('contains')) {
	function contains($needles, $haystack) {
		return count(array_intersect($needles, explode(" ", preg_replace("/[^A-Za-z0-9' -]/", "", $haystack))));
	}
}

if (! function_exists('random_terms')) {
	function random_terms($lines, $total) {
		shuffle($lines);
		return array_slice($lines, 0, $total);
	}
}

if (! function_exists('limit_text')) {
	function limit_text($text, $limit) {
		if (str_word_count($text, 0) > $limit) {
			$words = str_word_count($text, 2);
			$pos = array_keys($words);
			$text = substr($text, 0, $pos[$limit]) . '...';
		}
		return title_case($text);
	}
}

if (! function_exists('unslug')) {
	function unslug($slug) {
		return str_replace('-', ' ', $slug);
	}
}

