<!-- meta seo -->
@if( \Route::is('index') )

  <title>{{ ucwords(implode(' - ', random_terms($data['keyword'], 2))) }}</title>
  <meta name="description" content="{{ title_case($data['keyword'][0]) }} - {{ implode(' ', random_terms($data['keyword'], 20)) }}"/>
  <meta name="keyword" content="{{ title_case($data['keyword'][0]) }} - {{ implode(', ', random_terms($data['keyword'], 20)) }}"/>



@elseif( \Route::is('search') )

  <title>{{ strtoupper(unslug($data['query'])) }} - {{ ucwords(implode(' - ', random_terms($data['keyword'], 2))) }}</title>
  <meta name="description" content="{{ title_case(unslug($data['query'])) }} - {{ implode(' ', $data['related']) }}"/>
  <meta name="keyword" content="{{ title_case(unslug($data['query'])) }} - {{ implode(', ', $data['related']) }}"/>

@elseif( \Route::is('attachment') )

  <title>{{ strtoupper(unslug($data['subquery'])) }} - {{ title_case(unslug($data['query'])) }}</title>
  <meta name="description" content="{{ title_case(unslug($data['query'])) }} - {{ implode(' ', $data['related']) }}"/>
  <meta name="keyword" content="{{ title_case(unslug($data['query'])) }} - {{ implode(', ', $data['related']) }}"/>

@elseif( \Route::is('page') )

  <title>{{ $data['page_title'] }} | {{ ucwords(implode(' - ', random_terms($data['keyword'], 2))) }}</title>
  <meta name="description" content="{{ title_case($data['keyword'][0]) }} - {{ implode(' ', random_terms($data['keyword'], 20)) }}"/>
  <meta name="keyword" content="{{ title_case($data['keyword'][0]) }} - {{ implode(', ', random_terms($data['keyword'], 20)) }}"/>


@endif


<!-- meta robots -->
