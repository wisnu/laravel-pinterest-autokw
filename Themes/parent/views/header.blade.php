<script>
    var beforeload = (new Date()).getTime();
</script>
@include('meta')
	<meta charset="utf-8">
	<meta name="MobileOptimized" content="320">
	<meta name="HandheldFriendly" content="True">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta itemprop="url" content="{{ url()->current() }}" />
	<meta content="index,follow,imageindex" name="googlebot">
	<meta content="all,index,follow" name="robots">
	<meta content="all,index,follow" name="googlebot-Image">
	<meta content="width=device-width,initial-scale=1" name="viewport">
	<meta content="general" name="rating">
	<meta content="global" name="distribution">
	<meta content="text/html; charset=utf-8" http-equiv="content-type">
	<meta content="en" name="language">
	<meta content="#FF6600" name="msapplication-TileColor">
	<meta content="{{ url()->current() }}" name="application-name">
	<meta content="{{ url()->current() }}" name="msapplication-tooltip">
	<meta name="google-site-verification" content="{{ config('custom.google')}}" />
	<meta name="msvalidate.01" content="{{ config('custom.bing') }}" />
	<meta name="yandex-verification" content="{{ config('custom.yandex') }}" />
	<link href="{{ url()->current() }}" rel="canonical">
	<link href="{{ themes('img/favicon.png') }}" rel="shortcut icon" type="image/x-icon"/>
	<link href="{{ themes('img/favicon.png') }}" rel="apple-touch-icon"/>
