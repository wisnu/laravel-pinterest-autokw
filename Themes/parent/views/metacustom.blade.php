<?php

$data = [
  'title'     => config('site.title'),
  'description' => config('site.description'),  
  'query'     => isset($query) ? $query : '',   
  'subquery'    => isset($subquery) ? $subquery : '',   
  'results'   => isset($results) ? $results : '',
  'random_terms'  => isset($random_terms) ? $random_terms : '',
  'page_title'  => isset($page_title) ? $page_title : '',
  'list_title'  => isset($list_title) ? $list_title : '',
];
     
?>
<!-- meta seo -->
@if( route('index') )

  <title>{{ seo_meta( config('site.index.title'), $data) }}</title>
  <meta name="description" content="{{ seo_meta( config('site.index.description'), $data) }}"/>

@elseif( \Route::is('search') )

  <title>{{ seo_meta( config('site.search.title'), $data) }}</title>
  <meta name="description" content="{{ seo_meta( config('site.search.description'), $data) }}"/>

@if( route('attachment') )

  <title>{{ seo_meta( config('site.attachment.title'), $data) }}</title>
  <meta name="description" content="{{ seo_meta( config('site.attachment.description'), $data) }}"/>

@if( route('page') )

  <title>{{ seo_meta( config('site.page.title'), $data) }}</title>
  <meta name="description" content="{{ seo_meta( config('site.page.description'), $data) }}"/>

@elseif( is_list() )

  <title>{{ seo_meta( config('site.list.title'), $data) }}</title>
  <meta name="description" content="{{ seo_meta( config('site.list.description'), $data) }}"/>

@endif


<!-- meta robots -->
