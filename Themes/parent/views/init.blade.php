<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $_SERVER['HTTP_HOST'] }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ themes('css/app.css') }}">
        <link rel="stylesheet" href="{{ themes('css/bootstrap.min.css') }}">
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @if (Auth::check())
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ url('/login') }}">Login</a>
                        <a href="{{ url('/register') }}">Register</a>
                    @endif
                </div>
            @endif

            <div class="content">

				<form class="form-inline" method="post" action="{{ action('Controller@saveKeyword') }}" enctype="multipart/form-data">
					<div class="form-group mb-2">
						<input class="form-control" name="keyword" type="text" value="{{ config('custom.keyword') }}">
						{{ csrf_field() }}

					</div>
					<div class="form-group mx-sm-3 mb-2">
						<button type="submit" class="btn btn-primary mb-2">Confirm</button>
					</div>

				</form>
						<i>Masukkan keyword utama diatas.</i><br>
						<i>Kalo ingin keyword lebih banyak, masuk console dan :</i>
						<pre>php artisan tools 'domain.tld' 'main keyword here' deep </pre>

            </div>
        </div>
    </body>
</html>
