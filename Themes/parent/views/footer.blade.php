<center><small><span id="pageload"></span></small></center>

@if( config('custom.stats.histats') )
    <div id="histats_counter"></div>
    <script type="text/javascript">var _Hasync= _Hasync|| [];
    _Hasync.push(['Histats.startgif', '1,{{ config('custom.stats.histats') }},4,10047,"div#histatsC {position: absolute;top:0px;left:0px;}body>div#histatsC {position: fixed;}"']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
    var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
    hs.src = ('//s10.histats.com/js15_gif_as.js');
    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();
    </script>
    <noscript>
      <a href="/" alt="hitcounter" target="_blank" >
        <div id="histatsC">
          <img alt="histats" src="http://s4is.histats.com/stats/i/{{ config('custom.stats.histats') }}.gif?{{ config('custom.stats.histats') }}&103">
        </div>
      </a>
    </noscript>
@endif

@if( config('custom.stats.ganalytics') )

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111747717-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', '{{ config('custom.stats.google_analytic') }}');
</script>
@endif

@if( config('custom.stats.statcounter.sc_project') )
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project={{ config('custom.stats.statcounter.sc_project') }};
var sc_invisible=1;
var sc_security="{{ config('custom.stats.statcounter.sc_security') }}";
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript>
  <div class="statcounter">
    <a title="web stats"
  href="http://statcounter.com/" target="_blank">
  <img class="statcounter" src="//c.statcounter.com/{{ config('custom.stats.statcounter.sc_project') }}/0/{{ config('custom.stats.statcounter.sc_security') }}/1/" alt="webstats"></a>
  </div>
</noscript>
<!-- End of StatCounter Code for Default Guide -->
@endif

<script type="text/javascript">

  var afterload = (new Date()).getTime();
  seconds = (afterload-beforeload) / 1000;
  document.getElementById("pageload").innerHTML = 'Page load time :  ' + seconds + ' sec(s)';

function init() {
  var imgDefer = document.getElementsByTagName('img');
  for (var i=0; i<imgDefer.length; i++) {
    if(imgDefer[i].getAttribute('data-src')) {
      imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
    }
  }



}
window.onload = init;

</script>
 