@extends('layout')

@section('content')
<!-- bloc-1 -->
<div class="bloc l-bloc bg-repeat" id="bloc-1">
	<div class="container bloc-lg">
		<div class="row voffset-clear-xs">
			<div class="col-12">
				<a href="{{ url('/'.str_slug($data['query'])) }}" title="" rel="tag" />
					<h2 class="mg-md">{{ title_case($data['query']) }}</h2>
				</a>
				<img src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][0]['data'][0]['url'] }}" data-src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][0]['data'][0]['url'] }}" class="img-fluid mx-auto d-block lazyload" alt="{{ $data['data'][0]['data'][0]['title'] }}" width="960" height="640" />
			</div>
		</div>
		<div class="row voffset-clear-xs voffset">
			@for ($i=1; $i <=4; $i++)
			<div class="col-6 col-sm-3">
				<img src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][0]['data'][$i]['url'] }}" data-src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][0]['data'][$i]['url'] }}" class="img-fluid mx-auto d-block lazyload" alt="{{ $data['data'][0]['data'][$i]['title'] }}" width="217" height="145" />
			</div>
			@endfor
		</div>
	</div>
</div>
<!-- bloc-1 END -->

<!-- bloc-2 -->
<div class="bloc l-bloc none" id="bloc-2">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col-sm-9">
				<div class="row">
					@php
					$total = 100;
					for ($j=1; $j <= 3; $j++) {
					$rows = array_slice($data['keyword'], (($total * $j) - $total), $total);
					@endphp
					<div class="col">
					@foreach ($rows as $row)
						<a href="{{ url(str_slug($row)) }}" class="a-btn a-block" title="{{ title_case($row) }} image">{{ title_case($row) }}</a>
					@endforeach
					</div>
					@php
					}
					@endphp
				</div>
			</div>
			<div class="col-sm-3">
				<p>
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- bloc-2 END -->

<!-- bloc-3 -->
<div class="bloc l-bloc" id="bloc-3">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<h3 class="mg-md text-center">Gallery</h3>
				<p class="text-center">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit.&nbsp;
				</p>
			</div>
		</div>
		
		<div class="row voffset">
		
		@for ($k=1; $k<=8; $k++)
			<div class="col-lg-3 col-md-6">
				<a href="#" data-lightbox="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][1]['data'][$k]['url'] }}" data-gallery-id="gallery-1"><img src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][1]['data'][$k]['url'] }}" data-src="http://{{ $_SERVER['HTTP_HOST'] }}/img/{{ $data['data'][1]['data'][$k]['url'] }}" class="img-fluid mx-auto d-block lazyload" /></a>
				<h4 class="mg-md">{{ $data['data'][1]['data'][$k]['title'] }}</h4>
			</div>
		@endfor
		</div>
	</div>
</div>
<!-- bloc-3 END -->

<!-- bloc-4 -->
<div class="bloc bgc-white l-bloc" id="bloc-4">
	<div class="container bloc-md">
		<div class="row">
			<div class="col-6 col-sm-3">
				<img src="{{ themes('img/lazyload-ph.png') }}" data-src="{{ themes('img/ny-times.png') }}" class="img-fluid mx-auto d-block lazyload" />
			</div>
			<div class="col-6 col-sm-3">
				<img src="{{ themes('img/lazyload-ph.png') }}" data-src="{{ themes('img/forbes.png') }}" class="img-fluid mx-auto d-block lazyload" />
			</div>
			<div class="col-6 col-sm-3">
				<img src="{{ themes('img/lazyload-ph.png') }}" data-src="{{ themes('img/wired.png') }}" class="img-fluid mx-auto d-block lazyload" />
			</div>
			<div class="col-6 col-sm-3">
				<img src="{{ themes('img/lazyload-ph.png') }}" data-src="{{ themes('img/guardian.png') }}" class="img-fluid mx-auto d-block lazyload" />
			</div>
		</div>
	</div>
</div>
<!-- bloc-4 END -->
@endsection
