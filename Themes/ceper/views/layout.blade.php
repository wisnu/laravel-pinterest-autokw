<!doctype html>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#  place: http://ogp.me/ns/place#">
@include('header')

    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="stylesheet" type="text/css" href="{{ themes('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ themes('css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ themes('css/font-awesome.min.css') }}">
</head>
<body>
<!-- Main container -->
<div class="page-container">
    
<!-- bloc-0 -->
<div class="bloc l-bloc" id="bloc-0">
	<div class="container bloc-sm">
		<div class="row">
			<div class="col">
				<nav class="navbar navbar-light row navbar-expand-md" role="navigation">
					<a class="navbar-brand" href="{{ url('/') }}">{{ strtoupper($_SERVER['HTTP_HOST']) }}</a>
					<button id="nav-toggle" type="button" class="ml-auto ui-navbar-toggler navbar-toggler border-0 p-0" data-toggle="collapse" data-target=".navbar-1" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse navbar-1">
						<ul class="site-navigation nav navbar-nav ml-auto">
							<li class="nav-item">
								<a href="{{ url('/') }}" class="nav-link">Home</a>
							</li>
							<li class="nav-item">
								<a href="index.html" class="nav-link a-btn">Link</a>
							</li>
							<li class="nav-item">
								<a href="index.html" class="nav-link a-btn">Link</a>
							</li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>
<!-- bloc-0 END -->

@yield('content')

<!-- ScrollToTop Button -->
<a class="bloc-button btn btn-d scrollToTop" onclick="scrollToTarget('1',this)"><span class="fa fa-chevron-up"></span></a>
<!-- ScrollToTop Button END-->


<!-- bloc-9 -->
<div class="bloc l-bloc" id="bloc-9">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-12 col-sm-4">
				<h4 class="mg-md text-sm-left text-center">
					About
				</h4><a href="{{ url('page/disclaimer') }}" class="a-btn a-block footer-link">Disclaimer</a><a href="{{ url('page/contact') }}" class="a-btn a-block footer-link">Contact Us</a>
			</div>
			<div class="col-12 col-sm-4">
				<h4 class="mg-md text-sm-left text-center">
					Contents
				</h4><a href="{{ url('page/privacy') }}" class="a-btn a-block footer-link">Privacy</a><a href="{{ url('page/copyright') }}" class="a-btn a-block footer-link">Copyright</a><a href="{{ url('page/terms') }}" class="a-btn a-block footer-link">Terms</a>
			</div>
			<div class="col-12 col-sm-4">
				<h4 class="mg-md text-sm-left text-center">
					Follow Us
				</h4><a href="{{ url('page/disclaimer') }}" class="a-btn a-block footer-link">Twitter</a><a href="index.html" class="a-btn a-block footer-link">Facebook</a>
			</div>
		</div>
	</div>
</div>
<!-- bloc-9 END -->

</div>
<!-- Main container END -->
    

<script src="{{ themes('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ themes('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ themes('js/blocs.min.js') }}"></script>
<script src="{{ themes('js/jquery.touchSwipe.min.js') }}" defer></script>
<script src="{{ themes('js/lazysizes.min.js') }}" defer></script>
<!-- Additional JS END -->



<!-- Preloader -->
<div id="page-loading-blocs-notifaction" class="page-preloader"></div>
<!-- Preloader END -->
		@include('footer')
</body>
</html>
