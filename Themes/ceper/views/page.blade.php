@extends('layout')

@section('content')


<!-- bloc-8 -->
<div class="bloc l-bloc " id="bloc-8">
	<div class="container bloc-lg">
		<div class="row">
			<div class="col-sm-8">
				<header>
					<h2>{{ $data['page_title'] }}</h2>
				</header>
				<article>
					@include('page/' . strtolower($data['page_title']) )
				</article>
			</div>
			<div class="col-sm-4">
			</div>
		</div>
	</div>
</div>
<!-- bloc-8 END -->
@endsection