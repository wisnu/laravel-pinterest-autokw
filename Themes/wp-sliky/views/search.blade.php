@extends('layout')

@section('content')


<h1>{{ $data['data'][0]['keyword'] }}</h1>

<div class="imagegallery">

@foreach($data['data'][0]['data'] as $item)

<div class="ithumb">
  <a href="{{ attachment_url($data[0]['keyword'], $item['title']) }}" title="{{ title_case($item['title']) }}">
    <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ title_case($item['title']) }}" alt="{{ title_case($item['title']) }}">
  </a>
  <p>{{ limit_text($item['title'],4) }} <a target="_blank" href="{{ attachment_url($data[0]['keyword'], $item['title']) }}">.</a></p>
</div>


@endforeach

</div>

@endsection
