@extends('layout')


@section('title')
{{ $data['keyword'][0] }} 
@endsection

@section('content')

<div class="post list">
	<h1>Welcome : @yield('title')</h1>
	@foreach ($data['keyword'] as $i)
		<li><a title="{{ title_case($i) }}" href="{{ url(str_slug($i)) }}" >{{ limit_text($i, 4) }}</a></li>
	@endforeach
</div>

@endsection
