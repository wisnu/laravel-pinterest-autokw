@extends('layout')

@section('content')


<h1>{{ title_case(str_replace('-', ' ',$data['subquery'])) }} - {{ title_case(str_replace('-', ' ',$data['query'])) }}</h1>
            <center>
                {!! config('money.ads.336') !!}
            </center>
            <br>
            <h2>This image has been removed at the request of its copyright owner</h2>
            <br>

<div style="position:absolute;left:-999999px;top:-999999px;">
	<div class="imagegallery">
	
		<div class="singlet">
			<span class="adagambar">
			<a href="{{ attachment_url($data['title'], $data['data'][$data['id']]['title']) }}" title="{{ title_case(str_replace('-', ' ',$data['query'])) }} {{ $data['data'][$data['id']]['title'] }}">
			  <img style="width:100%" class="attachment-large size-large" data-src="{{ $data['data'][$data['id']]['url'] }}" src="{{ $data['data'][$data['id']]['url'] }}" alt="{{ $data['data'][$data['id']]['title'] }}" title="{{ $data['data'][$data['id']]['title'] }}"></a>
			</span>
	      <center>{{ $data['data'][$data['id']]['title'] }}</center>
		</div>
	
	    <div align="left"><p style="text-align: left">{!! nl2br($data['content']) !!}</p></div>
	
	                <div style="padding: 15px 0 15px">
	                  <b>Tags: </b>
	                   @foreach ($data['related'] as $rel)
	                     <a href="{{ url(str_slug($data['title'])) }}" title="{{ title_case($rel) }}">{{ title_case($rel) }}</a> &nbsp; | &nbsp;
	                   @endforeach
	                </div>
	
		<h3>Gallery of {{ $data['subquery'] }}</h3>
	
	@foreach ($data['data'] as $item)
	      <div class="ithumb">
	        <a href="{{ attachment_url($data['title'], $item['title']) }}" title="{{ title_case(str_replace('-', ' ',$data['query'])) }} {{ $item['title'] }}">
	          <img class="class_th" src="{{ $item['small'] }}" data-src="{{ $item['url'] }}" onerror="this.onerror=null;this.src='{{ $item['small'] }}';" width="225" height="100" title="{{ title_case(str_replace('-', ' ',$data['query'])) }} {{ $item['title'] }}" alt="{{ title_case(str_replace('-', ' ',$data['query'])) }} {{ $item['title'] }}">
	        </a>
	        <p>{{ title_case(str_replace('-', ' ',$data['query'])) }} - {{ $item['title'] }} <a target="_blank" href="{{ $item['url'] }}">.</a></p>
	      </div>
	@endforeach
	
	</div>


</div>
            <center>
                {!! config('money.ads.336') !!}
            </center>
@endsection
