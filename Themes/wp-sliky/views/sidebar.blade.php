<div id="sidebar">

@if( !\Route::is('page' ) )
	{!! config('custom.ads.responsive') !!}	

<p class="label">Recent Posts</p>
<ul>
@foreach ($data['related'] as $terms)

		<li><a href="{{ url(str_slug($terms)) }}" title="{{ $terms }}">{{ title_case($terms) }}</a></li>

@endforeach											
</ul>
@endif
</div>