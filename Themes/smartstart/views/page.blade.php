@extends('layout')

@section('content')

<section id="content" class="container clearfix">

	<section id="main">

		<article class="entry single clearfix">

			<div class="entry-body">

				<a href="#">
					<h1 class="title">{{ $data['page_title'] }}</h1>
				</a>

				<p>
					@include('page/' . strtolower($data['page_title']) )
				</p>


				
			</div><!-- end .entry-body -->

			
		</article><!-- end .entry -->
		
	</section><!-- end #main -->

@include('sidebar')
	
</section><!-- end #content -->
@endsection