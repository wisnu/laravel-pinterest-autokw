	<aside id="sidebar">

		<div class="widget">

			<h6 class="widget-title">Related Post</h6>

			<ul class="categories">
				@foreach ($data['keyword'] as $rel)
				<li><a href="{{ url(str_slug($rel)) }}" title="{{ title_case($rel) }}">{{ title_case($rel) }}</a></li>
				@endforeach

			</ul>

		</div><!-- end .widget -->
		
		@if (isset($data['path']) != 'page')
		<div class="widget">
		
			<h6 class="widget-title">Tags</h6>

			<p>{{ ucwords(implode(' ', $data['related'])) }}</p>

		</div><!-- end .widget -->
		@endif

		
	</aside><!-- end #sidebar -->
