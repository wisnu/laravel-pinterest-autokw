<!DOCTYPE html>

<html class="not-ie no-js" lang="en"> 
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#  place: http://ogp.me/ns/place#">
		@yield('header')		
	
		<link rel="stylesheet" href="{{ themes('css/style.css') }}" media="screen" />

		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,300,800,700,400italic%7CPT+Serif:400,400italic" />
		
		<link rel="stylesheet" href="{{ themes('css/fancybox.min.css') }}" media="screen" />

		<script href="{{ themes('js/modernizr.custom.js') }}"></script>


</head>
<body>

<header id="header" class="container clearfix">

	<a href="{{ url('/') }}" id="logo">
		<h1>{{ strtoupper($_SERVER['HTTP_HOST']) }}</h1>
	</a>

	<nav id="main-nav">
		
		<ul>
			<li class="current">
				<a href="{{ url('/') }}" data-description="All starts here">Home</a>
			</li>
			<li>
				<a href="{{ url('page/contact') }}" data-description="Enquire here">Contact</a>
			</li>
		</ul>

	</nav><!-- end #main-nav -->
	
</header><!-- end #header -->

@yield('content')

<footer id="footer" class="clearfix">

	<div class="container">

		<div class="three-fourth">

			<nav id="footer-nav" class="clearfix">

				<ul>
					<li><a href="{{ url('/') }}">Home</a></li>
					@foreach (config('custom.page') as $page)
					<li><a href="{{ url('page/'.str_slug($page)) }}">{{ title_case($page) }}</a></li>
					@endforeach
				</ul>
				
			</nav><!-- end #footer-nav -->

			<ul class="contact-info">
				<li class="address">2525  Dola Mine Road, Durham NC 27713. USA</li>
				<li class="phone">(919) 316-6355</li>
				<li class="email"><a href="{{ url('page/contact') }}">mail me here</a></li>
			</ul><!-- end .contact-info -->
			
		</div><!-- end .three-fourth -->

		<div class="one-fourth last">

			<span class="title">Stay connected</span>

			<ul class="social-links">
				<li class="twitter"><a href="https://twitter.com/wisnu">Twitter</a></li>
				<li class="facebook"><a href="#">Facebook</a></li>
				<li class="digg"><a href="#">Digg</a></li>
				<li class="vimeo"><a href="#">Vimeo</a></li>
				<li class="youtube"><a href="#">YouTube</a></li>
				<li class="skype"><a href="#">Skype</a></li>
			</ul><!-- end .social-links -->

		</div><!-- end .one-fourth.last -->
		
	</div><!-- end .container -->

</footer><!-- end #footer -->

<footer id="footer-bottom" class="clearfix">

	<div class="container">

		<ul>
			<li>{{ $_SERVER['HTTP_HOST'] }} &copy; {{ date('Y') }}</li>
			<li><a href="#">Legal Notice</a></li>
			<li><a href="#">Terms</a></li>
		</ul>

	</div><!-- end .container -->

</footer><!-- end #footer-bottom -->

<!--[if !lte IE 6]><!-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script href="{{ themes('js/jquery-1.7.1.min.js') }}"><\/script>')</script>
	<!--[if lt IE 9]> <script href="{{ themes('js/selectivizr-and-extra-selectors.min.js') }}"></script> <![endif]-->
	<script href="{{ themes('js/jquery.ui.widget.min.js') }}"></script>
	<script href="{{ themes('js/respond.min.js') }}"></script>
	<script href="{{ themes('js/jquery.isotope.min.js') }}"></script>
	<script href="{{ themes('js/jquery.touchSwipe.min.js') }}"></script>
	<script href="{{ themes('js/custom.js') }}"></script>
<!--<![endif]-->
@include('footer')
</body>
</html>