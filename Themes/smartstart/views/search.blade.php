@extends('layout')

@section('content')

<section id="content" class="container clearfix">

	<section id="main">

		<article class="entry single clearfix">

			<a href="{{ url()->current() }}" title="{{ $data['data'][0]['title'] }}">
				<img src="{{ theImage('full', $data['data'][0]['title'], $data['data'][0]['url'], 680, 360) }}" alt="{{ $data['data'][0]['title'] }}" class="entry-image" width="680px">
			</a>

			<div class="entry-body">

				<a href="{{ url()->current() }}">
					<h1 class="title">{{ $data['data'][0]['title'] }}</h1>
				</a>

				<p>{{ $data['data'][0]['description'] }} {{ $data['data'][0]['caption'] }}<p>


				
			</div><!-- end .entry-body -->

			
		</article><!-- end .entry -->

		<div class="clear"></div>

		<ul class="projects-carousel clearfix">
			@foreach ($data['data'] as $single)
			<li>
				<a href="{{ url(str_slug($data['query']).'/'.str_slug($single['title'])) }}.html" title="{{ $single['title'] }}" rel="tag" title="{{ $single['title'] }} images">
					<img src="{{ theImage('thumb', $single['title'], $single['url'], 219, 140) }}" alt="{{ $single['title'] }} images" data-src="{{ theImage('thumb', $single['title'], $single['url'], 219, 140) }}">
					<h5 class="title">{{ limit_text($single['title'], 3) }}</h5>
					<span class="categories">{{ limit_text($single['description'], 4) }}</span>
				</a>
			</li>

			@endforeach
		

	
		</ul><!-- end .projects-carousel -->

		
	</section><!-- end #main -->

@include('sidebar')
	
</section><!-- end #content -->
@endsection