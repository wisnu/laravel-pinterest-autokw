// @extends('layout')

@section('content')
	
<section id="content" class="container clearfix">

	<h2 class="slogan align-center">{{ $data['data'][0]['description'] }} {{ $data['data'][0]['caption'] }}</h2>
	
	<section id="photos-slider" class="ss-slider">
	
		<article class="slide">
			<img src="{{ theImage('thumb', $data['data'][0]['title'], $data['data'][0]['url'], 940, 360) }}" alt="{{ $data['data'][0]['description'] }}" class="slide-bg-image" />

			
			
		</article><!-- end .slide -->
	
		
	</section><!-- end #photos-slider -->

	<h6 class="section-title">Latest Blog</h6>

	<ul class="projects-carousel clearfix">
		@for ($k=1; $k<=8; $k++)
		<li>
			<a href="{{ url(str_slug($data['data'][$k]['title'])) }}" title="{{ $data['data'][$k]['title'] }}" rel="tag" title="{{ $data['data'][$k]['title'] }} images">
				<img src="{{ theImage('thumb', $data['data'][$k]['title'], $data['data'][$k]['url'], 219, 140) }}" alt="{{ $data['data'][$k]['title'] }} images" data-src="{{ theImage('thumb', $data['data'][$k]['title'], $data['data'][$k]['url'], 219, 140) }}">
				<h5 class="title">{{ limit_text($data['data'][$k]['title'], 3) }}</h5>
				<span class="categories">{{ limit_text($data['data'][$k]['description'], 4) }}</span>
			</a>
		</li>

		@endfor
	</ul><!-- end .projects-carousel -->

	<h6 class="section-title">Latest stuff from our blog</h6>


	<div class="clear"></div>

	<hr />
	@php
		$total = 100;
		for ($j=1; $j <= 4; $j++) {
		$rows = array_slice($data['keyword'], (($total * $j) - $total), $total);
	@endphp
	<div class="one-fourth last">

	@foreach ($rows as $row)
		<ul class="arrow dotted">
			<li><a href="{{ url(str_slug($row)) }}" title="{{ title_case($row) }} image">{{ limit_text(title_case($row), 4) }}</a>
			</li>
		</ul>
	@endforeach

	</div>
	@php
	}
	@endphp

 	
</section><!-- end #content -->
@endsection