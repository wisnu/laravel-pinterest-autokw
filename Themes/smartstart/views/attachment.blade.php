@extends('layout')

@section('content')

<section id="content" class="container clearfix">

	<section id="main">

		<article class="entry single clearfix">

			<a href="{{ url()->current() }}" title="{{ $data['data'][$data['id']]['title'] }}">
				<img src="{{ theImage('full', $data['data'][$data['id']]['title'], $data['data'][$data['id']]['url'], 680, 360) }}" alt="{{ $data['data'][$data['id']]['title'] }}" class="entry-image" width="680px">
			</a>

			<div class="entry-body">

				<a href="#">
					<h1 class="title">{{ $data['data'][$data['id']]['title'] }}</h1>
				</a>

				<p>{{ $data['data'][$data['id']]['description'] }} {{ $data['data'][$data['id']]['caption'] }}<p>


				
			</div><!-- end .entry-body -->

			
		</article><!-- end .entry -->

		<div class="clear"></div>

		<ul class="projects-carousel clearfix">
			@foreach ($data['data'] as $single)
			<li>
				<a href="{{ url(str_slug($data['query']).'/'.str_slug($single['title'])) }}.html" title="{{ $single['title'] }}" rel="tag" title="{{ $single['title'] }} images">
					<img src="{{ theImage('thumb', $single['title'], $single['url'], 219, 140) }}" alt="{{ $single['title'] }} images" data-src="{{ theImage('thumb', $single['title'], $single['url'], 219, 140) }}">
					<h5 class="title">{{ limit_text($single['title'], 3) }}</h5>
					<span class="categories">{{ limit_text($single['description'], 4) }}</span>
				</a>
			</li>

			@endforeach
		

	
		</ul><!-- end .projects-carousel -->

		
	</section><!-- end #main -->

@include('sidebar')
	
</section><!-- end #content -->
@endsection