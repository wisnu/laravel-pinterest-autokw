<?php

return [
	'keyword' => 'healthy dinner recipes',
	
	'token' => 'pn5cvnnbO7Fc81Cf4OKBLw3KXcED3O3lB8Zvs8ZvfKufd1cxHVSfhw8pxhYkoF9k',	//ask me
	
	'theme' => 'smartstart',
	
  	'email' => 'wisnu.hendro@gmail.com',
  	
	'page' => [ "disclaimer", "privacy", "copyright", "terms", "contact" ], // list name pages

//	stats & tracking
	'google'  => 'hTBcjHCR2LSkadGkNUIY8tPQwkkVg6MlcJunEqy2-rk',
	
	'bing' => 'FE5861948FFACECFB3F8431348436A93',
	
	'yandex' => 'fe12daddfa1af679',
	
  	'stats' => [
          'histats' => '',
          'ganalytics' => 'UA-XXX',
          'gtm' => '',
          'statcounter'       => [
              'sc_project'      => null,
              'sc_security'     => null,
          ],
        ],
        
	'ads' => [
		'links' => '',
		'responsive' => '',
		'leaderboard' => '',
		//etc
	],

//	images and contents
	'image' => 'hotlink',		// 	hotlink / saved / wrapped
	
	'imageloc' => 'local',		//	cloud or local

	'imgproxy' => 'i0.wp.com',	// images.weserv.nl or i{0-2}.wp.com

	'content' => false,
];