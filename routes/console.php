<?php

use Illuminate\Foundation\Inspiring;
use app\Console\Commands\Tools;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('tools {domain} {keyword} {deep}', function ($domain, $keyword, $deep) {
		Tools::saveKeyword($domain, $keyword, $deep);
})->describe("Generate deep list. use: tools 'domain' 'keyword' 'deep'");