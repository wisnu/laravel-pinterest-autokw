<?php
use Illuminate\Support\Facades\Input;


Route::get('/', 						'Controller@index')->where('page', '[1-9]+[0-9]*')->name('index');
Route::post('/', 						'Controller@saveKeyword');

Route::get('/page/{pagename}', 			'Controller@page')->name('page');
Route::get('/thumb/{w}/{h}/{catchall?}', 	'Controller@thumb')->where('catchall', '.*');
Route::get('/full/{catchall?}', 			'Controller@full')->where('catchall', '.*');

Route::get('/img/{path}', function($path, League\Glide\Server $server, Illuminate\Http\Request $request) {
    $server->outputImage($path, $request->all());
})->where('path', '.+');


Route::get('/{query}', 					'Controller@search')->where('page', '[1-9]+[0-9]*')->name('search');
Route::get('/{query}/{subquery}.html', 	'Controller@attachment')->name('attachment');
